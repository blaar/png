
#ifndef PNG_TOOLS_H
#define PNG_TOOLS_H
#include "blc_array.h"

/**
\defgroup png png functions
\{
 Functions to read and write png files. Image conversions can be done during the process.
 */

/** 
 Read the png file and set the properties of the image.
 \param image where to put the result
 \param filename name of the file to use
 \param requested_type 'NDEF', 'UIN8' or 'UI16'
 \param requested_format 'NDEF', 'Y800', 'RGB3' or 'RGBA'
 
 Depending on the type and format of the png, a conversion may be done.
 If 'NDEF' is used, the final blc_array will be set with the native type and format of the png.
 The memory is not allocated.*/
START_EXTERN_C
#ifdef __cplusplus
void blc_image_def_with_png_file(blc_array *image, char const* filename, uint32_t requested_type='NDEF', uint32_t requested_format='NDEF');
#else
void blc_image_def_with_png_file(blc_array *image, char const* filename, uint32_t requested_type, uint32_t requested_format);
#endif

/**Fill the blc_array with the pixels data of the png.
 The image must have been defined and allocated before.*/
void blc_image_update_with_png_file(blc_array *image, char const* filename);

/**Def the image, allocate it and update the png*/
#ifdef __cplusplus
void blc_image_init_with_png_file(blc_array *image, char const* filename,  uint32_t requested_type='NDEF', uint32_t requested_format='NDEF');
#else
void blc_image_init_with_png_file(blc_array *image, char const* filename,  uint32_t requested_type, uint32_t requested_format);
#endif

/**Create a new png file and save the **blc_array** in it.*/
void blc_image_save_png_file(blc_array *image, char const* filename);
END_EXTERN_C
///\}
#endif
