#include "../png_tools.h"

#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h> //getpid

#define DEFAULT_OUTPUT_NAME ":i_pngfile<pid>" //':'

/**
 We update the name by replacing eventual '<pid>' by the real pid.
 We check the name to be a valid blc_channel name. If yes we retrun 1, 0 otherwise.*/
int check_or_update_blc_channel_name(char const**name){
    char const*pid_pos;
    char *new_name;
    int pos;
    
    pid_pos=strstr(*name, "<pid>");
    if (pid_pos) {
        asprintf(&new_name, "%.*s%d", (int)(pid_pos-*name), *name, getpid());
        *name=new_name;
    }
    
    SYSTEM_ERROR_CHECK(sscanf(*name, "%*[:/.^]%*[^/]%n", &pos), -1, "Checking '%s' for blc_channel name", new_name);
    //  printf("ret:%d pos:%d  strlen:%d\n", ret, pos, strlen(*name));
    if (pos==strlen(*name)) return 1;
    else return 0;
}

int main(int argc, char**argv){
    blc_channel output;
    char final_filename[PATH_MAX];
    char const *filename, *output_name, *type_str, *format_str, *period_str, *images_nb_str;
    int period, images_nb, image_id;
    uint32_t format, type;
    
    blc_program_set_description("Load a png as an image");
    blc_program_add_option(&format_str, 'f', "format", "NDEF|Y800|RGB3|RGBA", "format of the desired image (NDEF for native format of the png)", "NDEF");
    blc_program_add_option(&images_nb_str, 'n', "number", "integer", "number of images to load (need: file format like: filename%d.png)", NULL);
    blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "Name of the channel", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&period_str, 'p', "period", "integer", "refresh period in ms (-1 for blocking)", "-1");
    blc_program_add_option(&type_str, 't', "type", "NDEF|UIN8|INT8|FL32", "type of the data (NDEF for native type of png)", "NDEF");
    blc_program_add_parameter(&filename, "filename", 1, "filename of the image", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    
    if (filename==NULL){
        blc_program_args_display_help();
        EXIT_ON_ERROR("You need to specify a filename: -F<filename>");
    }
    format=STRING_TO_UINT32(format_str);
    type=STRING_TO_UINT32(type_str);
    
    SSCANF(1, period_str, "%d", &period);
    if (period==-1) period=-2; //We do not block before the first image
    else period*=1000; //convert in microseconds
    
    if (check_or_update_blc_channel_name(&output_name)==0) EXIT_ON_ERROR("'%s' is not a valid channel name", output_name);
    
    if (images_nb_str) {
        SSCANF(1, images_nb_str, "%d", &images_nb);
        image_id=0;
        SPRINTF(final_filename, filename, image_id);
        image_id++;
    }
    else {
        STRCPY(final_filename, filename);
        images_nb=1;
    }
    
    //Only the properties are defined, the data is not allocated.
    blc_image_def_with_png_file(&output, final_filename, type, format);
    //Now we know the properties (size, format, ...) we allocate the shared memory.
    output.create_or_open(output_name, BLC_CHANNEL_WRITE);
    output.publish();
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);
    
    BLC_COMMAND_LOOP(period){
        //If we have a number of images to load and we have not finish
        blc_image_update_with_png_file(&output, final_filename);
        if (images_nb_str && image_id != images_nb-1) {
            SPRINTF(final_filename, filename, image_id);
            image_id++;
        }
        
    }
    
    
    return EXIT_SUCCESS;
}
