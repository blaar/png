cmake_minimum_required(VERSION 2.6)
project(i_pngfile)

find_package(blc_channel REQUIRED)
find_package(blc_image REQUIRED)
find_package(blc_program REQUIRED)
find_package(PNG REQUIRED)

add_definitions(${BL_DEFINITIONS})
include_directories(${BL_INCLUDE_DIRS} ${PNG_INCLUDE_DIRS})
add_executable(i_pngfile i_pngfile.cpp  ../png_tools.cpp)
target_link_libraries(i_pngfile ${BL_LIBRARIES} ${PNG_LIBRARIES})
