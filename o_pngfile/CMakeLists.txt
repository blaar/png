cmake_minimum_required(VERSION 2.6)
project(o_pngfile)

find_package(blc_channel REQUIRED)
find_package(blc_image REQUIRED)
find_package(blc_program REQUIRED)
find_package(PNG REQUIRED)

add_definitions(${BL_DEFINITIONS})
include_directories(${BL_INCLUDE_DIRS} ${PNG_INCLUDE_DIRS})
add_executable(o_pngfile o_pngfile.cpp ../png_tools.cpp)
target_link_libraries(o_pngfile ${BL_LIBRARIES} ${PNG_LIBRARIES})
