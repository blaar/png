#include "../png_tools.h"

#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h> //getpid

int main(int argc, char**argv){
    blc_channel input;
    char final_filename[PATH_MAX];
    char const *filename, *input_name, *type_str, *period_str, *images_nb_str;
    int period, images_nb, image_id;
    uint32_t type;
    
    blc_program_set_description("Image to a png file");
    blc_program_add_option(&filename, 'f', "file", "filename", "file containing the file", NULL);
    blc_program_add_option(&images_nb_str, 'n', "number", "integer", "number of images to load (need: file format like: filename%d.png)", NULL);
    blc_program_add_option(&period_str, 'p', "period", "integer", "period between each sample in ms (0 as fast as possible)", "0");
    blc_program_add_option(&type_str, 't', "type", "INT8|FL32", "type of the data. ('NDEF' for same type as input)", "NDEF");
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "image to save", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    
    if (filename==NULL){
        blc_program_args_display_help();
        EXIT_ON_ERROR("You need to specify a filename: -f<filename>");
    }
    SSCANF(1, period_str, "%d", &period);
    type=STRING_TO_UINT32(type_str);
    
    if (images_nb_str) {
        SSCANF(1, images_nb_str, "%d", &images_nb);
        image_id=0;
    }
    else {
        STRCPY(final_filename, filename);
        images_nb=1;
    }
    
    input.open(input_name, BLC_CHANNEL_READ);
    //Synchronize the loop with the channel
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);
    
    BLC_COMMAND_LOOP(period*1000){
        if (images_nb_str && ( image_id != images_nb)){
			SPRINTF(final_filename, filename, image_id);
			image_id++;
		} else blc_command_ask_quit();
		
        blc_image_save_png_file(&input, final_filename);
    }
    
    return EXIT_SUCCESS;
}
