/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
 
 *
 *
 *  Created on: August 1, 2015
 *      Author: Arnaud Blanchard
 */
#include "png_tools.h"
#include "blc_image.h"
 
#include <png.h>
#include <zlib.h>
#ifndef PNG_ERROR_ACTION_NONE  //old libpng
#define PNG_ERROR_ACTION_NONE 1
#endif

static FILE* open_png_file_and_init_header(png_struct **png_ptr, png_info **info_ptr, char const *filename){
    FILE* file;

    SYSTEM_ERROR_CHECK(file=fopen(filename, "rb"), NULL, "Opening '%s'", filename);
 
    *png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL,   NULL);
    if (!png_ptr) EXIT_ON_ERROR("allocating png struct");
    
    *info_ptr = png_create_info_struct(*png_ptr);
    if (!info_ptr) {
        png_destroy_read_struct(png_ptr, NULL, NULL);
        EXIT_ON_ERROR("allocating info_png struct");
    }
    
    png_init_io(*png_ptr, file);
    png_set_sig_bytes(*png_ptr, 0);
    png_read_info(*png_ptr, *info_ptr);
    return file;

}

//Get the properties of the png image. If type or format is 'NDEF' it is set to the default value of the image. If it is defined the image is converted if possible.
static void get_png_properties(png_struct *png_ptr,  png_info *info_ptr, png_uint_32 *width, png_uint_32 *height, uint32_t *type, uint32_t *format){
    int bit_depth, color_type;
    uint32_t type_str, format_str;
    
    png_get_IHDR(png_ptr, info_ptr, width, height, &bit_depth, &color_type, NULL, NULL, NULL);

    
    //Select and eventually convert the type of data
    if (bit_depth <= 8) {
        switch (*type){
            case 'NDEF': *type='UIN8';
            case 'UIN8':
                if (bit_depth!=8) png_set_expand_gray_1_2_4_to_8(png_ptr);
                break;
            case 'UI16':
#ifdef PNG_READ_EXPAND_16_SUPPORTED
                png_set_expand_16(png_ptr);
#else
                EXIT_ON_ERROR("this version of libpng does not support png_set_expand_16");
#endif
                break;
            default:EXIT_ON_ERROR( "The bit depth is '%d' your type '%4s' is not correct.", bit_depth,  UINT32_TO_STRING(type_str, *type));
        }
    }else  switch (*type){
            case 'NDEF': *type='UI16';
            break;
            case 'UI16':
            break;
            case 'UIN8': png_set_strip_16(png_ptr);
            break;
        default:EXIT_ON_ERROR( "The bit depth id '%d' your type '%4s' is not correct.", UINT32_TO_STRING(type_str, *type));
    }
    
    //Select and eventually convert the format of data
    switch (color_type){
        case PNG_COLOR_TYPE_GRAY:
            switch (*format){
                case 'NDEF':*format='Y800';
                    break;
                case 'Y800':
                    break;
                case 'RGB3':png_set_gray_to_rgb(png_ptr);
                case 'RGBA':
                    png_set_add_alpha(png_ptr, 255, PNG_FILLER_AFTER);
                    break;
                default:EXIT_ON_ERROR("The format can not be converted from PNG_COLOR_TYPE_GRAY to '%.4s'.", UINT32_TO_STRING(format_str, *format));
            }
            break;
        case PNG_COLOR_TYPE_RGB:
            switch (*format){
                case 'NDEF':*format='RGB3';
                    break;
                case 'Y800': png_set_rgb_to_gray(png_ptr, PNG_ERROR_ACTION_NONE, 0.0, 0.0);
                    break;
                case 'RGB3':
                    break;
                case 'RGBA':png_set_add_alpha(png_ptr, 255, PNG_FILLER_AFTER);
                    break;
                default:EXIT_ON_ERROR("The format can not be converted from PNG_COLOR_TYPE_RGB to '%.4s'.", UINT32_TO_STRING(format_str, *format));
            }
            break;
        case PNG_COLOR_TYPE_PALETTE:
            switch (*format){
                case 'Y800':png_set_palette_to_rgb(png_ptr);
                    png_set_rgb_to_gray(png_ptr, PNG_ERROR_ACTION_NONE, 0.0, 0.0);
                    break;
                case 'NDEF':*format='RGB3';
                case 'RGB3':
                    png_set_palette_to_rgb(png_ptr);
                case 'RGBA':
                    png_set_add_alpha(png_ptr, 255, PNG_FILLER_AFTER);
                    break;
                default:EXIT_ON_ERROR("The format can not be converted from PNG_COLOR_TYPE_PALETTE to '%.4s'.", UINT32_TO_STRING(format_str, *format));
            }
            break;
        case PNG_COLOR_TYPE_RGBA:
            switch (*format){
                case 'Y800':png_set_rgb_to_gray(png_ptr, PNG_ERROR_ACTION_NONE, 0.0, 0.0);
                case 'RGB3':
                    EXIT_ON_ERROR("RGBA to RGB3 does not work (unknown problem with png_set_strip_alpha).\nYou should convert your file with image magic:\n    convert -background white -alpha remove white.png <your image>.png <converted image>.png");
                    png_set_strip_alpha(png_ptr);
                    break;
                case 'NDEF':*format='RGBA';
                case 'RGBA':                    break;
                default:EXIT_ON_ERROR("The format can not be converted from PNG_COLOR_TYPE_RGB_ALPHA to '%.4s'.", UINT32_TO_STRING(format_str, *format));
            }
            break;
        default:EXIT_ON_ERROR("The PNG_COLOR_TYPE '%d' can not be converted to '%.4s'.", color_type,  UINT32_TO_STRING(format_str, *format));
    }
}

void blc_image_def_with_png_file(blc_array *image, char const* filename, uint32_t requested_type, uint32_t requested_format){
    FILE *file;
    png_struct *png_ptr;
    png_info *info_ptr;
    png_uint_32 width, height;
    
    file = open_png_file_and_init_header(&png_ptr, &info_ptr, filename);
    get_png_properties(png_ptr, info_ptr, &width, &height, &requested_type, &requested_format);
    image->type=requested_type;
    image->format=requested_format;
    blc_image_def(image, requested_type, requested_format, width, height);
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(file);
}


void blc_image_update_with_png_file(blc_array *image, char const* filename){
    FILE *file;
    int i, line_width;
    png_uint_32 width, height;
    png_struct *png_ptr;
    png_info *info_ptr;
    png_byte **row_pointers;
    
    file = open_png_file_and_init_header(&png_ptr, &info_ptr, filename);
    get_png_properties(png_ptr, info_ptr, &width, &height, &image->type, &image->format);
    
    line_width=image->dims[image->dims_nb-1].step;
    
    height=image->dims[image->dims_nb-1].length;
    row_pointers=MANY_ALLOCATIONS(height, png_byte*);
    FOR_INV(i, height) row_pointers[i]=&(image->uchars[line_width*i]);
    png_read_image(png_ptr, row_pointers);
    FREE(row_pointers);
    png_read_end(png_ptr, info_ptr);

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(file);
}


/**Def the image, allocate it and update the png*/
void blc_image_init_with_png_file(blc_array *image, char const* filename,  uint32_t requested_type, uint32_t requested_format){
    FILE *file;
    int i, line_width;
    png_uint_32 width, height;
    png_struct *png_ptr;
    png_info *info_ptr;
    png_byte **row_pointers;
    
    file = open_png_file_and_init_header(&png_ptr, &info_ptr, filename);
    get_png_properties(png_ptr, info_ptr, &width, &height, &requested_type, &requested_format);
    image->type=requested_type;
    image->format=requested_format;
    blc_image_def(image, requested_type, requested_format, width, height);
    image->allocate();
    
    line_width=image->dims[image->dims_nb-1].step;
    
    height=image->dims[image->dims_nb-1].length;
    row_pointers=MANY_ALLOCATIONS(height, png_byte*);
    FOR_INV(i, height) row_pointers[i]=&(image->uchars[line_width*i]);
    png_read_image(png_ptr, row_pointers);
    FREE(row_pointers);
    png_read_end(png_ptr, info_ptr);
    
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(file);
}


void blc_image_save_png_file(blc_array *image, char const* filename){
    FILE *file;
    int i, line_width;
    png_info *info_ptr;
    png_struct *png_ptr;
    png_uint_32 width, height;
    png_byte bit_depth=0, color_type=0;
    png_byte **row_pointers;


    SYSTEM_ERROR_CHECK(file=fopen(filename, "wb"), NULL, "Saving png: filename:'%s'", filename);
    
    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr==NULL) EXIT_ON_ERROR("creatig png_struct");
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr==NULL){
        png_destroy_write_struct(&png_ptr,(png_infopp)NULL);
        EXIT_ON_ERROR("creatig png_infop");
    }
    png_init_io(png_ptr, file);
    /* set the zlib compression level */
    png_set_compression_level(png_ptr, Z_DEFAULT_COMPRESSION);
    
    width=image->dims[image->dims_nb-2].length;
    height=image->dims[image->dims_nb-1].length;
    
    switch(image->type){
        case 'UIN8': case 'INT8': bit_depth=8;
            break;
        case 'UI16': case 'IN16': bit_depth=16;
            break;
        default: EXIT_ON_ARRAY_ERROR(image, "data type not managed");
    }
    
    switch(image->format){
        case 'Y800': color_type=PNG_COLOR_TYPE_GRAY;
            break;
        case 'RGB3': color_type=PNG_COLOR_TYPE_RGB;
            break;
        case 'RGBA': color_type=PNG_COLOR_TYPE_RGB_ALPHA;
            break;
        default: EXIT_ON_ARRAY_ERROR(image, "image format not managed");
    }

    png_set_IHDR(png_ptr, info_ptr, width, height, bit_depth, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);
    line_width=image->dims[image->dims_nb-1].step;
    row_pointers=MANY_ALLOCATIONS(height, png_byte*);
    FOR_INV(i, height) row_pointers[i]=&image->uchars[line_width*i];
    png_write_image(png_ptr, row_pointers);
    FREE(row_pointers);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    
    SYSTEM_ERROR_CHECK(fclose(file), -1, "Closing '%s'", filename);
}





